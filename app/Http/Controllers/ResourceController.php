<?php
/**
 * Created by PhpStorm.
 * User: murali
 * Date: 20/8/16
 * Time: 1:14 PM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use OneSignal;
class ResourceController extends Controller
{

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware('jwt.auth');
    }

    /**
     * Return the user
     *
     * @return Response
     */
    public function index()
    {

        // Retrieve all the users in the database and return them
        $users = User::all();

        return $users;
    }

    /**
     * Return a JWT
     *
     * @return Response
     */
    public function getS3policyPlain()
    {
        define('AWS_ACCESS_KEY', 'AKIAJG6TJ45HH2OUU5SQ');
        define('AWS_SECRET', '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');
        $s3FormDetails = getS3Details('images.myschool24x7.com', 'ap-south-1');
        $data=[];
        foreach ($s3FormDetails['inputs'] as $name => $value) {
            $data[$name]=$value;
        }
        return response()->json(compact('data'));

    }
    public function getNotifications()
    {
        $Notifications = json_decode( OneSignal::getNotifications()->getBody()->getContents(), true );
        return response()->json(compact('Notifications'));

    }
    public function sendNotification(Request $request){
        $data=$request->input('data');
        $ss=[
            "include_player_ids"   =>  $data['playerID'],
            "contents"              => ["en" => $data['msg']],
            "headings"              => ["en" => $data['heading']!==NULL?$data['heading']:null],
            "small_icon"            =>isset($data['smallIcon'])?$data['smallIcon']:null,
            "large_icon"            =>isset($data['bigIcon'])?$data['bigIcon']:null,
            "big_picture"            =>isset($data['newsImage'])?$data['newsImage']:null,
            "data"                  =>isset($data['newsAbout'])?$data['newsAbout']:null,
            "priority:"             =>10,
            "url"                  =>isset($data['url'])?$data['url']:null
        ];
        return response()->json(compact('ss'));
        if(($data['playerID']!==NULL)&&($data['msg']!==NULL)){
            $response = OneSignal::postNotification([
                "include_player_ids"   =>  $data['playerID'],
                "contents"              => ["en" => $data['msg']],
                "headings"              => ["en" => $data['heading']!==NULL?$data['heading']:null],
                "small_icon"            =>isset($data['smallIcon'])?$data['smallIcon']:null,
                "large_icon"            =>isset($data['bigIcon'])?$data['bigIcon']:null,
                "big_picture"            =>isset($data['newsImage'])?$data['newsImage']:null,
                "data"                  =>isset($data['newsAbout'])?$data['newsAbout']:null,
                "priority:"             =>10,
                "url"                  =>isset($data['url'])?$data['url']:null
            ]);
            return response()->json(compact('response'));
        }
        else{
            return response()->json(array('error' =>'Data Null'), 404);
        }

    }
    public function sendNotificationToSegment(Request $request){
        $data=$request->input('data');
        $response = OneSignal::postNotification([
            "included_segments"   =>  $data['segments'],
            "contents"              => ["en" => $data['msg']],
            "headings"              => ["en" => $data['heading']?$data['heading']:null],
            "small_icon"            =>$data['smallIcon']?$data['smallIcon']:null,
            "large_icon"            =>$data['bigIcon']?$data['bigIcon']:null,
            "big_picture"            =>$data['newsImage']?$data['newsImage']:null,
            "data"                  =>$data['newsAbout']?$data['newsAbout']:null,
            "priority:"             =>10,
            "url"                  =>$data['url']?$data['url']:null
        ]);
        return response()->json(compact('response'));
    }

}


/**
 * Get all the necessary details to directly upload a private file to S3
 * asynchronously with JavaScript using the Signature V4.
 *
 * @param string $s3Bucket your bucket's name on s3.
 * @param string $region   the bucket's location/region, see here for details: http://amzn.to/1FtPG6r
 * @param string $acl      the visibility/permissions of your file, see details: http://amzn.to/18s9Gv7
 *
 * @return array ['url', 'inputs'] the forms url to s3 and any inputs the form will need.
 */
function getS3Details($s3Bucket, $region, $acl = 'public-read') {

    // Options and Settings
    $awsKey = (!empty(getenv('AWS_ACCESS_KEY')) ? getenv('AWS_ACCESS_KEY') : AWS_ACCESS_KEY);
    $awsSecret = (!empty(getenv('AWS_SECRET')) ? getenv('AWS_SECRET') : AWS_SECRET);

    $algorithm = "AWS4-HMAC-SHA256";
    $service = "s3";
    $date = gmdate("Ymd\THis\Z");
    $shortDate = gmdate("Ymd");
    $requestType = "aws4_request";
    $expires = "86400"; // 24 Hours
    $successStatus = "201";
    $url = "//{$s3Bucket}.{$service}-{$region}.amazonaws.com";

    // Step 1: Generate the Scope
    $scope = [
        $awsKey,
        $shortDate,
        $region,
        $service,
        $requestType
    ];
    $credentials = implode('/', $scope);

    // Step 2: Making a Base64 Policy
    $policy = [
        'expiration' => gmdate('Y-m-d\TG:i:s\Z', strtotime('+6 hours')),
        'conditions' => [
            ['bucket' => $s3Bucket],
            ['acl' => $acl],
            ['starts-with', '$key', ''],
            ['starts-with', '$Content-Type', ''],
            ['success_action_status' => $successStatus],
            ['x-amz-credential' => $credentials],
            ['x-amz-algorithm' => $algorithm],
            ['x-amz-date' => $date],
            ['x-amz-expires' => $expires],
        ]
    ];
    $base64Policy = base64_encode(json_encode($policy));

    // Step 3: Signing your Request (Making a Signature)
    $dateKey = hash_hmac('sha256', $shortDate, 'AWS4' . $awsSecret, true);
    $dateRegionKey = hash_hmac('sha256', $region, $dateKey, true);
    $dateRegionServiceKey = hash_hmac('sha256', $service, $dateRegionKey, true);
    $signingKey = hash_hmac('sha256', $requestType, $dateRegionServiceKey, true);

    $signature = hash_hmac('sha256', $base64Policy, $signingKey);

    // Step 4: Build form inputs
    // This is the data that will get sent with the form to S3
    $inputs = [
        'Content-Type' => '',
        'acl' => $acl,
        'success_action_status' => $successStatus,
        'policy' => $base64Policy,
        'X-amz-credential' => $credentials,
        'X-amz-algorithm' => $algorithm,
        'X-amz-date' => $date,
        'X-amz-expires' => $expires,
        'X-amz-signature' => $signature
    ];

    return compact('url', 'inputs');
}