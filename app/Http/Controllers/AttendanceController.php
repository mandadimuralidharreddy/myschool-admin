<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use DB;
use Carbon\Carbon;
/**
 * Class AttendanceController
 * @package App\Http\Controllers
 */


class AttendanceController extends Controller
{
    protected $table = 'schoolData';

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware('jwt.auth');
    }

    /**
     * Return the user
     *
     * @return Response
     */
    public function index()
    {

        // Retrieve all the users in the database and return them
        $users = User::all();

        return $users;
    }

    public function getAttendanceData(){
        $attendanceData = DB::select('select * from attendancetablemeta where schoolId = ? and FID=?', [auth()->user()->schoolCode,auth()->user()->id]);

        return response()->json(compact('attendanceData'));
    }
     public function getAttendanceDataByUID(){
         if((isset($_GET['UID']))){
             $ID=$_GET['UID'];
             $attendanceData = DB::select('select * from  attendancetable where schoolId = ?  and UID=?', [auth()->user()->schoolCode,$ID]);
             return response()->json(compact('attendanceData'));
         }
         else{
             return response()->json(array('error' =>'ID not supplied'), 404);
         }

    }
    public function deleteAttendanceDataByUID(){
        if((isset($_GET['UID']))){
            $ID=$_GET['UID'];
            DB::table('attendancetablemeta')->where('UID', '=', $ID)->delete();
            DB::table('attendancetable')->where('UID', '=', $ID)->delete();
        }


    }
    public function postAttendanceData(Request $request){
     $attendanceData=$request->input('data');
        $fileName=$request->input('fileName');
        $UID=uniqid();
        DB::beginTransaction();
        try {
            DB::table('attendancetablemeta')->insert(array(
                'UID'=>$UID,
                'FID'=>auth()->user()->id,
                'schoolId'=>auth()->user()->schoolCode,
                'name'=>$fileName,
                'timeStamp'=>Carbon::now('Asia/Kolkata')
            ));
            foreach ($attendanceData as $atten){
                DB::table('attendancetable')->insert(array(
                    'UID'=>$UID,
                    'schoolId'=>auth()->user()->schoolCode,
                    'adNo'=>$atten['adNo'],
                    'class'=>$atten['class'],
                    'section'=>$atten['section'],
                    'rollNo'=>$atten['rollNo'],
                    'date'=>$atten['date'],
                    'status'=>$atten['status'],
                    'edittedBy'=>''
                ));
            }
            DB::commit();
            return response()->json(array('success' =>'Insert Completed'), 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(array('error' =>'Insert Failed.Check your data and try again'.$e), 404);
        }
    }
    public function putAttendanceData(Request $request){
        $attendanceData=$request->input('data');
        $fileName=$request->input('fileName');
        $UID=$request->input('UID');
        DB::beginTransaction();
        try {
            DB::table('attendancetablemeta')
                ->where(['UID'=> $UID])
                ->update(['name' => $fileName]);
            if(sizeof($attendanceData)>=1){
                foreach ($attendanceData as $atten){
                    DB::table('attendancetable')
                        ->where(['id'=> $atten['id']])
                        ->update(['UID'=>$UID,
                            'schoolId'=>auth()->user()->schoolCode,
                            'adNo'=>$atten['adNo'],
                            'class'=>$atten['class'],
                            'section'=>$atten['section'],
                            'rollNo'=>$atten['rollNo'],
                            'date'=>$atten['date'],
                            'status'=>$atten['status'],
                            'edittedBy'=>auth()->user()->id]);
                }
            }

            DB::commit();
            return response()->json(array('success' =>'Update Completed'), 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(array('error' =>'Insert Failed.Check your data and try again'.$e), 404);
        }
    }
    public function isValidAttendance($data){

    }
 }