<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use DB;
use Carbon\Carbon;
/**
 * Class SchoolController
 * @package App\Http\Controllers
 */


class SchoolController extends Controller
{
    protected $table = 'schoolData';

    public function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        $this->middleware('jwt.auth');
    }

    /**
     * Return the user
     *
     * @return Response
     */
    public function index()
    {

        // Retrieve all the users in the database and return them
        $users = User::all();

        return $users;
    }

    /**
     * Return a JWT
     *
     * @return Response
     */
    public function getSchoolData()
    {
//        DB::transaction(function () {
//            DB::table('users')->update(['votes' => 1]);
//
//            DB::table('posts')->delete();
//        });
        /** @var  $schoolData */


        $schoolData = DB::select('select ID,schoolCode,schoolBranch,welcomeData,schoolLogo,schoolName from schoolData where schoolCode = ?', [auth()->user()->schoolCode]);
        return response()->json(compact('schoolData'));
    }
    public function postSchoolData()
    {

    }
    /**
     *put data in database
     */
    public function putSchoolData(Request $request)
    {
        $schoolData=$request->input('data');
        $schoolNewData =DB::table('schoolData')
            ->where(['schoolCode'=> auth()->user()->schoolCode,'ID'=>$schoolData['ID']])
            ->update(['schoolBranch' => $schoolData['schoolBranch'],'welcomeData' => $schoolData['welcomeData'],
            'schoolLogo' => $schoolData['schoolLogo'],'schoolName' => $schoolData['schoolName']]);
        return response()->json(compact('schoolNewData'));
    }
    public function getnewsData(){
        $schoolNews = DB::select('select * from news where schoolCode = ? and status=0', [auth()->user()->schoolCode]);
        return response()->json(compact('schoolNews'));
    }
    public function putnewsData(Request $request){
        $news=$request->input('data');
        $newsNewData =DB::table('news')
            ->where(['schoolCode'=> auth()->user()->schoolCode,'id'=>$news['id']])
            ->update([
                'status'=>1]);
        return response()->json(compact('newsNewData'));
    }
    public function postnewsData(Request $request){
        $news=$request->input('data');
        if(($news['newsHeading']!==NULL)&&($news['newsWhom']!==NULL)&&($news['description']!==NULL)&&($news['imageURL']!==NULL)&&($news['sendType']!==NULL)){

            if($news['newsWhom']=="some"){
                if(($news['groups']!==NULL)){
                     $ss= DB::table('news')->insertGetId(array(
                
                    'CID'=>auth()->user()->id,
                     'schoolCode'=> auth()->user()->schoolCode,
                    'newsImageURL'=>$news['imageURL'],
                     'headline'=>$news['newsHeading'],
                    'news'=>$news['description'],
                    'forAll'=>0,
                    'groups'=>json_encode($news['groups']),
                    'timestamp'=>Carbon::now('Asia/Kolkata'),
                    'isEditted' => 0,
                    'edittedBy'=>null,
                    'edittedTime'=>null,
                    'status'=>0,
                    'noOfLikes'=>0
                    
                ));
                return response()->json(compact('ss'));
                }
               
            }
            else if($news['newsWhom']=="all"){
                  $ss= DB::table('news')->insertGetId(array(
                 'CID'=>auth()->user()->id,
                     'schoolCode'=> auth()->user()->schoolCode,
                    'newsImageURL'=>$news['imageURL'],
                     'headline'=>$news['newsHeading'],
                    'news'=>$news['description'],
                    'forAll'=>1,
                    'groups'=>0,
                    'timestamp'=>Carbon::now('Asia/Kolkata'),
                    'isEditted' => 0,
                    'edittedBy'=>null,
                    'edittedTime'=>null,
                    'status'=>0,
                    'noOfLikes'=>0
                  ));
                return response()->json(compact('ss'));
            }
            
        }
       
               
//        DB::table('news')->insert(
//                ['isEditted' => 0,'news'=>$news['news'],
//                'forAll'=>$news['forAll'],'headline'=>$news['headline'],'newsImageURL'=>$news['newsImageURL'],'news'=>$news['news'],
//                'status'=>1]
           
        //return response()->json(compact('news'));
    }
    public function getEventsData(){
        $events = DB::select('select * from events where schoolCode = ? and status=1', [auth()->user()->schoolCode]);
    
        return response()->json(compact('events'));
    }
    public function postEventsData(Request $request){
        $eventData=$request->input('data');
        if(($eventData['eventDate']!==NULL)&&($eventData['eventName']!==NULL)&&($eventData['imageData']!==NULL)){
            $ss= DB::table('events')->insertGetId(array(     
                    'eventName'=>$eventData['eventName'],
                    'eventDate'=>$eventData['eventDate'],
                     'images'=>json_encode($eventData['imageData']),
                    'timeStamp'=>Carbon::now('Asia/Kolkata'),
                    'editted'=>0,
                    'uploadedBy'=>auth()->user()->id,
                'schoolCode'=> auth()->user()->schoolCode,
                'status'=>1
                ));
                return response()->json(compact('ss'));
        }
    }
    public function putEventsData(Request $request){
         $event=$request->input('data');
        $eventNewData =DB::table('events')
            ->where(['schoolCode'=> auth()->user()->schoolCode,'id'=>$event['id']])
            ->update([
                'status'=>0]);
        return response()->json(compact('eventNewData'));
    }
}
