/**
 * Created by murali on 24/8/16.
 */
(function() {

    'use strict';

    angular
        .module('authApp')
        .factory('userData',['$http',function ($http) {
            return function(message) {
                return $http.get('api/authenticate/user')


            }
        }])

})();