(function() {

	'use strict';
//.module('authApp', ['ui.router', 'satellizer','papa-promise','ngSanitize','ngFileUpload','blueimp.fileupload','monospaced.elastic'])
	angular
		.module('authApp', ['ui.router', 'satellizer','papa-promise','ngSanitize','ngFileUpload','blockUI','angular-loading-bar', '720kb.datepicker','ui.tinymce','monospaced.elastic','oitozero.ngSweetAlert','ui.bootstrap','ngImgCrop','ngImageCompress','ui-notification','ui.select2','ngCsvImport'])
		.config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide,blockUIConfig) {


			// Satellizer configuration that specifies which API
			// route the JWT should be retrieved from
			$authProvider.loginUrl = '/api/authenticate';
			blockUIConfig.delay = 0.001;
			blockUIConfig.message = 'Working Please wait....';
			// Redirect to the auth state if any other states
			// are requested other than users
			$urlRouterProvider.otherwise('/login');
		//	$httpProvider.interceptors.push('xmlHttpInterceptor');
			$stateProvider
				.state('login', {
					url: '/login',
					templateUrl: '../views/authView.html',
					controller: 'AuthController as auth'
				})
				.state('profile', {
					url: '/profile',
					templateUrl: 'index.html',
					controller: 'profileCtrl'
				})
				.state('welcome', {
					url: '/welcome',
					templateUrl: 'views/welcome.html',
					controller: 'welcomeCtrl'
				})
				.state('news', {
					url: '/news',
					templateUrl: 'views/news.html',
					controller: 'newsCtrl'
				})
				.state('events', {
					url: '/events',
					templateUrl: 'views/events.html',
					controller: 'eventsCtrl'
				})
				.state('results', {
					url: '/results',
					templateUrl: 'views/results.html',
					controller: 'resultsCtrl'
				})
				.state('attendance', {
					url: '/attendance',
					templateUrl: 'views/attendance.html',
					controller: 'attendanceCtrl'
				})
			function redirectWhenLoggedOut($q, $injector,$rootScope) {

				return {

					responseError: function(rejection) {

						// Need to use $injector.get to bring in $state or else we get
						// a circular dependency error
						var $state = $injector.get('$state');

						// Instead of checking for a status code of 400 which might be used
						// for other reasons in Laravel, we check for the specific rejection
						// reasons to tell us if we need to redirect to the login state
						var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

						// Loop through each rejection reason and redirect to the login
						// state if one is encountered
						angular.forEach(rejectionReasons, function(value, key) {
							if(rejection.data.error === value) {

								// If we get a rejection corresponding to one of the reasons
								// in our array, we know we need to authenticate the user so
								// we can remove the current user from local storage
								localStorage.removeItem('user');

								// Send the user to the auth state so they can login
								$state.go('login');
							}

						});
						$rootScope.$broadcast('loading:error',rejection.data);
						return $q.reject(rejection);
					}
				}
			}

			// Setup for the $httpInterceptor
			$provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

			// Push the new factory onto the $http interceptor array
			$httpProvider.interceptors.push('redirectWhenLoggedOut');
			// $httpProvider.interceptors.push(function ($rootScope) {
			// 	return {
			// 		request: function (config) {
			// 			$rootScope.$broadcast('loading:show');
			// 			return config
			// 		},
			// 		response: function (response) {
			// 			$rootScope.$broadcast('loading:hide');
			// 			return response
			// 		},
			// 		requestError: function (response) {
			// 			// do something on error
			// 			$rootScope.$broadcast('loading:error');
			// 			return response
			// 		}, responseError: function (response) {
			// 			// do something on error
			// 			$rootScope.$broadcast('loading:error');
			// 			return response
			// 		}
            //
			// 	}
			// });
			$authProvider.loginUrl = '/api/authenticate';
		})
		.run(function($rootScope,$auth,$http,Notification){
			$rootScope.data={};
			$rootScope.data.showPreviewBtn = false;
			$rootScope.$on('loading:error', function (event, args) {
				console.log(args)
				Notification.error({message: args.error});
			});
			$rootScope.logout = function() {
				$http.get("api/authenticate/logout")
						.success(function(data){
							$auth.logout().then(function() {

								// Remove the authenticated user from local storage
								localStorage.removeItem('user');

								// Flip authenticated to false so that we no longer
								// show UI elements dependant on the user being logged in
								$rootScope.authenticated = false;

								// Remove the current user info from rootscope
								$rootScope.currentUser = null;
							});
						})

			}
			$rootScope.getUsers = function() {

				// This request will hit the index method in the AuthenticateController
				// on the Laravel side and will return the list of users
				$http.get('api/authenticate').success(function(users) {
					$rootScope.users = users;
				}).error(function(error) {
					$rootScope.error = error;
				});
			}
			if(localStorage.getItem('satellizer_token')) {
				$rootScope.authenticated = true

			}
			$rootScope.getUsers();
		})
		.directive('isActiveNav', [ '$location', function($location) {
				return {
					restrict: 'A',
					link: function(scope, element) {
						scope.location = $location;
						scope.$watch('location.path()', function(currentPath) {
							currentPath.split("/");
							var res = currentPath.split("/");
							res=res[1];
							if(res) {
								if ('/' + res === element[0].attributes['href'].nodeValue) {
									element.addClass('active');
								} else {
									element.removeClass('active');
								}
							}
						});
					}
				};
			}])
		.directive('fileInput', ['$parse', function ($parse) {
			return {
				restrict: 'A',
				link: function (scope, element, attributes) {
					element.bind('change', function () {
						$parse(attributes.fileInput)
								.assign(scope,element[0].files)
						scope.$apply()
					});
				}
			};
		}])
		.directive('onReadFile', function ($parse) {
			return {
				restrict: 'A',
				scope: {conSec: '=', current: '='},
				link: function(scope, element, attrs) {
					element.on('change', function(onChangeEvent) {

						var file = (onChangeEvent.srcElement || onChangeEvent.target).files[0];

						Papa.parse(file, {
							header: true,
							worker: true,

							complete: function(results) {
								if(scope.conSec.name){
									scope.conSec.fileInput.err = results.err;
									scope.conSec.fileInput.data = results.data;
									scope.conSec.fileInput.meta = results.meta;
									// Need to force the update
									scope.$apply();
								}
							}
						});
					});
				}
			};
		})
		.filter('propsFilter', function() {
			return function(items, props) {
				var out = [];

				if (angular.isArray(items)) {
					var keys = Object.keys(props);

					items.forEach(function(item) {
						var itemMatches = false;

						for (var i = 0; i < keys.length; i++) {
							var prop = keys[i];
							var text = props[prop].toLowerCase();
							if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
								itemMatches = true;
								break;
							}
						}

						if (itemMatches) {
							out.push(item);
						}
					});
				} else {
					// Let the output be the input untouched
					out = items;
				}

				return out;
			};
		})

})();
