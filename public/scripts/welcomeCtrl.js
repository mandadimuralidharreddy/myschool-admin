(function() {

    'use strict';

    angular
        .module('authApp')
        .controller('welcomeCtrl',function ($scope,$http, $auth,$rootScope,$uibModal, $state,userData,SweetAlert,Notification) {
            $rootScope.data.showPreviewBtn = true;
            $scope.data={};
            $scope.data.schoolData={};
            userData().success(function(data){
                    $rootScope.isLogin=true;
                    $rootScope.currentUser=data.user;
                })
                .error(function(res){
                    $state.go('login');
                });
            $http.get('api/schoolData/welcome')
                .success(function(data){
                    $scope.data.schoolData=data.schoolData;
                    $scope.data.welcomeData=JSON.parse($scope.data.schoolData[0].welcomeData.slice(10));
                    console.log($scope.data.welcomeData);
                });
            var notiData={"data":{
                "playerID":"50da1bf8-489e-4843-8ca5-dbcf25760e00",
                "heading":"Hi",
                "msg":"Hello"
            }};
            $http({
                url:"/api/getResourceData/sendNotification",
                method:"post",
                data:notiData
                })
                .success(function (data) {
                   console.log(data);
                })
                .error(function(res){
                });
            $scope.addNewFeature=function(){
                var newFuture={'objectiveList':''};
                $scope.data.welcomeData.features.push(newFuture);
            };
            $scope.removeFuture=function(id){
                var length=$scope.data.welcomeData.features.length;
                if(length>1){
                    SweetAlert.swal({   title: "Are you sure?",
                            text: "You will not be able to recover this field !",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false },
                        function(){
                            $scope.data.welcomeData.features.splice(id,1);
                            SweetAlert.swal("Deleted!", "Your field has been deleted.", "success");

                        });
                }
                else{
                    SweetAlert.swal("Warning", "There must be at least one filed.", "info");
                }

            };
            $scope.removeFiled= function (id) {
                var length=$scope.data.welcomeData.data.length;
                if(length>1) {
                    SweetAlert.swal({
                            title: "Are you sure?",
                            text: "You will not be able to recover this field !",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false
                        },
                        function () {
                            SweetAlert.swal("Deleted!", "Your field has been deleted.", "success");
                            $scope.data.welcomeData.data.splice(id, 1);
                        });
                }
                else{
                    SweetAlert.swal("Warning", "There must be at least one filed.", "info");
                }

            };
            $scope.removeFiledPoint=function (k,id) {
                if($scope.data.welcomeData.data[k]['data'].length >1)
                $scope.data.welcomeData.data[k]['data'].splice(id, 1);
                else
                    $scope.data.welcomeData.data.splice(k, 1)
            }
            $scope.addNewFiled=function(filedName){

                var data=[""];
                var newFiled={'title':filedName,'data':data};
                $scope.data.welcomeData.data.push(newFiled);
                $scope.data.addnewFiledName=false;
            };
            $scope.showNewFiled=function(){
                $scope.data.addnewFiledName=true;
            };
            $scope.changeImage=function(dataObj,imagePath,schoolImgID){
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'imageUpload.html',
                    windowClass: 'app-modal-window',
                    keyboard: false,
                    modalFade: true,
                    scope: $scope,
                    controller: function ($scope, $http,Upload,$timeout) {
                        $http.get('/api/getResourceData/getS3policyPlain')
                            .success(function (data) {
                                $scope.data.formDataPlain=data.data;
                            })
                            .error(function(res){
                            });
                        $scope.submitImage=function(file){
                            if(imagePath ==='schoolImg')
                                Upload.resize(file, {width: 120, height: 80})
                                    .then(function(resizedFile){
                                        file=resizedFile;

                                    });
                            Upload.upload({
                                url: 'http://images.myschool24x7.com/',
                                method: 'POST',
                                skipAuthorization: true,
                                data:{
                                    key:new Date().toISOString()+'_'+file.name,
                                    'Content-Type': file.type !== '' ? file.type : 'application/octet-stream',
                                    'acl': $scope.data.formDataPlain['acl'],
                                    'success_action_status':$scope.data.formDataPlain['success_action_status'],
                                    'policy': $scope.data.formDataPlain['policy'],
                                    'X-amz-credential':$scope.data.formDataPlain['X-amz-credential'],
                                    'X-amz-algorithm':$scope.data.formDataPlain['X-amz-algorithm'],
                                    'X-amz-date':$scope.data.formDataPlain['X-amz-date'],
                                    'X-amz-expires': $scope.data.formDataPlain['X-amz-expires'],
                                    'X-amz-signature': $scope.data.formDataPlain['X-amz-signature'],
                                    file:file
                                }
                            })
                                .then(function (response) {
                                    var x2js = new X2JS();
                                    var json = x2js.xml_str2json( response.data);
                                    if(imagePath ==='schoolLogo')
                                        dataObj[imagePath]=json.PostResponse.Location;
                                    else if(imagePath ==='schoolImg')
                                        dataObj[schoolImgID].schoolImg=json.PostResponse.Location;
                                    modalInstance.dismiss();
                                    $timeout(function () {
                                        file.result = response.data;
                                    });
                                }, function (response) {
                                    $scope.errorMsg = response.status + ': ' + response.data;
                                }, function (evt) {
                                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                                });

                        }
                        $scope.closeModal=function(){
                            modalInstance.dismiss();
                        }
                    }
                });
            }
            $rootScope.showSchoolInfo=function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/welcomePreview.html',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false,
                    windowClass: "modal fade in",
                    scope: $scope,
                    controller: function ($scope,$rootScope,$uibModalInstance,$http) {
                        $scope.saveData=function () {
                            console.log($scope.data.welcomeData)
                            $scope.data.schoolData[0].welcomeData='"welcome":'+JSON.stringify($scope.data.welcomeData);
                            $http.put('/api/schoolData/welcome',{"data":$scope.data.schoolData[0]})
                                .success(function (res) {
                                    Notification.success({message: 'Update Success'});
                                    $state.reload();
                                    $uibModalInstance.dismiss('cancel');
                                })
                                .error(function (data) {
                                    console.log(data)
                                })
                        }
                        $scope.cancel=function () {
                            $uibModalInstance.dismiss('cancel');
                        }
                    }

                });
            }
        })

})();