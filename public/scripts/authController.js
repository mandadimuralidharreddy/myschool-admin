(function() {

	'use strict';

	angular
			.module('authApp')
			.controller('AuthController', AuthController);


	function AuthController($auth, $state, $http, $rootScope,$scope) {

		var vm = this;

		vm.loginError = false;
		vm.loginErrorText;
		$rootScope.isLogin=false;
		vm.login = function() {

			var credentials = {
				email: vm.email,
				password: vm.password
			}

			$auth.login(credentials).then(function() {

				// Return an $http request for the now authenticated
				// user so that we can flatten the promise chain
				return $http.get('api/authenticate/user');

				// Handle errors
			}, function(error) {
				vm.loginError = true;
				vm.loginErrorText = error.data.error;

				// Because we returned the $http.get request in the $auth.login
				// promise, we can chain the next promise to the end here
			}).then(function(response) {

				// Stringify the returned data to prepare it
				// to go into local storage
				var user = JSON.stringify(response.data.user);

				// Set the stringified user data into local storage
				localStorage.setItem('user', user);
				// Everything worked out so we can now redirect to
				// the users state to view the data
				$state.go('welcome');
			});
		}
	}

})();