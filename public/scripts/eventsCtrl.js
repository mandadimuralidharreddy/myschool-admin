(function() {

    'use strict';

    angular
        .module('authApp')
        .controller('eventsCtrl', function ($scope,$http, $auth, $rootScope,$compile,userData,Upload,$timeout,blockUI,SweetAlert,$state) {
            $rootScope.data.showPreviewBtn = false;
            $scope.data={};
            $scope.data.formDataPlain={};
            $http.get('/api/getResourceData/getS3policyPlain')
                .success(function (data) {
                    $scope.data.formDataPlain=data.data;
                })
                .error(function(res){

                });
            userData().success(function(data){
                    $rootScope.isLogin=true;
                    $rootScope.currentUser=data.user;
                })
                .error(function(res){
                    $state.go('auth');
                });
            $http.get('/api/schoolData/getEventsData')
            .success(function(data){
                $scope.eventsData=data.events;
                angular.forEach($scope.eventsData,function(data,key){
                  
                 $scope.eventsData[key]['images']=JSON.parse($scope.eventsData[key]['images']);
                   
                });
                console.log($scope.eventsData);
            })
            .error(function(data){
                
            });
         $scope.deleteevent=function (data) {

                SweetAlert.swal({
                        title: "Are you sure?",
                        text: "This Event item will not show in app",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                    data.status=0;
                        $http.put('api/schoolData/getEventsData',{"data":data})
                            .success(function (data) {
                                 SweetAlert.swal("Deleted!", "Your NEWS has been deleted.", "success");
                           
                            })
                            .error(function (res) {

                            })
                       

                    });
            };
            $scope.formData={};
            $scope.imagePath={};
            $scope.imagePathThumb={};
            $scope.uploadImageCount = undefined;
            $scope.uploadEventName = undefined;
            $scope.uploadEventDate = undefined;
            $scope.startBlock=function(){
                blockUI.start();
            };
            $scope.stopBlock=function(){
                blockUI.stop();
            };
            $scope.submit=function(data,formData){
                $scope.uploadImageCount=data.length;
                $scope.uploadEventName = formData.eventName;
                $scope.uploadEventDate = formData.eventDate;
                if(data){
                        if(formData.eventName !== null ||formData.eventDate !==null || data.length >=1){
                            blockUI.start();
                        angular.forEach(data, function(image) {
                            Upload.imageDimensions(image)
                                .then(function(dimensions){
                                    if(dimensions.width >1000 || dimensions.height>1000){
                                        Upload.resize(image, {width: 1000, height: 1000})
                                            .then(function(resizedFile){
                                                $scope.submitImage(resizedFile,blockUI,resizedFile.name);
                                                Upload.resize(resizedFile, {width: 468, height: 250})
                                                    .then(function(resizedFile){
                                                        $scope.submitImageThumb(resizedFile,blockUI,resizedFile.name)
                                                    });
                                            });
                                    }
                                    else{
                                        $scope.submitImage(image,blockUI,image.name);
                                        if(dimensions.width >500 || dimensions.height>250) {
                                            Upload.resize(image, {width: 468, height: 250})
                                                .then(function (resizedFile) {
                                                    $scope.submitImageThumb(resizedFile, blockUI, resizedFile.name)
                                                });
                                        }
                                        else{
                                            $scope.submitImageThumb(image,blockUI,image.name)
                                        }
                                    }


                                });
                        });
                            blockUI.stop();


                    }
                    else{
                        $scope.errorMsg="Enter Event Name and Event Date";
                        console.log("Enter Event Name and Event Date")
                    }

                }
            };
            function sendData(){
                var finalData={};
                finalData["eventName"]=  $scope.uploadEventName;
                finalData["eventDate"]=  $scope.uploadEventDate;
                var imageData=[];
                console.log($scope.imagePathThumb)
                console.log($scope.imagePath)
                angular.forEach($scope.imagePath, function(image,key) {
                    console.log(key)
                    console.log(image)
                    console.log($scope.imagePathThumb)
                        var dd={};
                        dd["thumb"]=$scope.imagePathThumb[key];
                        dd["large"]=image;
                        imageData.push(dd);
                });
               finalData["imageData"]=imageData;
                $http.post('api/schoolData/getEventsData',{'data':finalData})
                .success(function(data){
                    $state.reload()
                })
            }
            var counter=1;
            $scope.submitImage=function(file,blockUI,imgKey){
                Upload.upload({
                    url: 'http://images.myschool24x7.com/',
                    method: 'POST',
                    skipAuthorization: true,
                    data:{
                        key:new Date().toISOString()+'_'+file.name,
                        'Content-Type': file.type !== '' ? file.type : 'application/octet-stream',
                        'acl': $scope.data.formDataPlain['acl'],
                        'success_action_status':$scope.data.formDataPlain['success_action_status'],
                        'policy': $scope.data.formDataPlain['policy'],
                        'X-amz-credential':$scope.data.formDataPlain['X-amz-credential'],
                        'X-amz-algorithm':$scope.data.formDataPlain['X-amz-algorithm'],
                        'X-amz-date':$scope.data.formDataPlain['X-amz-date'],
                        'X-amz-expires': $scope.data.formDataPlain['X-amz-expires'],
                        'X-amz-signature': $scope.data.formDataPlain['X-amz-signature'],
                        file:file
                    }
                }).then(function (response) {
                    var x2js = new X2JS();
                    var json = x2js.xml_str2json( response.data);
                    //var dd={};
                    //dd[imgKey]=json.PostResponse.Location;
                    $scope.imagePath[imgKey]=json.PostResponse.Location;
                    if( $scope.uploadImageCount==counter){
                        sendData();
                    }
                    counter++;
                    $timeout(function () {
                        file.result = response.data;
                    });
                }, function (response) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                })
            };
            $scope.submitImageThumb=function(file,blockUI,imgKey){
                Upload.upload({
                    url: 'http://images.myschool24x7.com/',
                    method: 'POST',
                    skipAuthorization: true,
                    data:{
                        key:new Date().toISOString()+'_'+file.name,
                        'Content-Type': file.type !== '' ? file.type : 'application/octet-stream',
                        'acl': $scope.data.formDataPlain['acl'],
                        'success_action_status':$scope.data.formDataPlain['success_action_status'],
                        'policy': $scope.data.formDataPlain['policy'],
                        'X-amz-credential':$scope.data.formDataPlain['X-amz-credential'],
                        'X-amz-algorithm':$scope.data.formDataPlain['X-amz-algorithm'],
                        'X-amz-date':$scope.data.formDataPlain['X-amz-date'],
                        'X-amz-expires': $scope.data.formDataPlain['X-amz-expires'],
                        'X-amz-signature': $scope.data.formDataPlain['X-amz-signature'],
                        file:file
                    }
                }).then(function (response) {
                    var x2js = new X2JS();
                    var json = x2js.xml_str2json( response.data);
                    //var dd={};
                    //dd[imgKey]=json.PostResponse.Location;
                    $scope.imagePathThumb[imgKey]=json.PostResponse.Location;
                    //console.log( $scope.imagePathThumb);

                    $timeout(function () {
                        file.result = response.data;
                    });
                }, function (response) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
        })

})();