(function() {

    'use strict';

    angular
        .module('authApp')
        .controller('newsCtrl', function ($http, $scope,$auth, $rootScope,userData,$sce,SweetAlert,Upload,$timeout,$uibModal,$state) {
            $rootScope.data.showPreviewBtn = false;
            $scope.newsdata={};
            $scope.select2Options = {
                allowClear:true
            };
            $scope.schoolGroups=["5_A","5_B","5_C","5_D"];
            $scope.tinymceOptions = {
                 inline: false,
                plugins : 'advlist autolink link lists charmap print preview',
                skin: 'lightgray',
                theme : 'modern',
                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
            };
            userData().success(function(data){
                    $rootScope.isLogin=true;
                    $rootScope.currentUser=data.user;
                })
                .error(function(res){
                    $state.go('auth');
                });
            $http.get('api/schoolData/news')
                .success(function (data) {
                    $scope.newsData=data.schoolNews;
                    console.log( $scope.newsData)
                })
                .error(function (res) {
                    console.log(res);
                });
            $scope.renderHtml = function (html_code) {
               // $scope.news = $filter('cut')(html_code, true, 200, ' ...');
                $scope.news = $sce.trustAsHtml(html_code);
                return $scope.news;
            };
            $scope.when = {
                /**
                 * Convert Dateish to js Date
                 * @param  mixed string|Date dateish date-like string for Date constructor
                 * @return Date js Date or null
                 */
                its: function(dateish) {
                    if (!dateish) {
                        return null;
                    }

                    if (Object.prototype.toString.call(dateish) === '[object Date]') {
                        return dateish;
                    }

                    var mysqlRegEx = /^(\d\d\d\d)-(\d?\d)-(\d?\d) (\d\d):(\d\d):(\d\d)$/g;
                    var mysqlMatch = mysqlRegEx.exec(dateish);

                    var d;

                    if (mysqlMatch !== null) {
                        d = new Date(Date.UTC(
                            mysqlMatch[1],
                            mysqlMatch[2] - 1,
                            mysqlMatch[3],
                            mysqlMatch[4],
                            mysqlMatch[5],
                            mysqlMatch[6]));
                    } else {
                        d = new Date(dateish);
                    }
                    return d;
                }

            };
            $scope.editNEWS=function (data) {

            };
            $scope.deleteNEWS=function (data) {

                SweetAlert.swal({
                        title: "Are you sure?",
                        text: "This NEWS item will not show in app",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function () {
                    data.status=1;
                        $http.put('api/schoolData/news',{"data":data})
                            .success(function (data) {
                                 SweetAlert.swal("Deleted!", "Your NEWS has been deleted.", "success");
                           
                            })
                            .error(function (res) {

                            })
                       

                    });
            };
        
            $scope.newNEWSSubmit=function (data) {
                $scope.previewData=data;
                if(data.myCroppedImage)
                    var file=dataURItoBlob(data.myCroppedImage);     
                else
                    SweetAlert.swal("Select Image!", "Please upload any image related to news ny clicking select picture", "info");
                if(typeof data.description === undefined)
                     SweetAlert.swal("NEWS Discription ", "Please enter NEWS discription.", "info");
                else{
                     var modalInstance = $uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'views/newsPreview.html',
                    windowClass: 'app-modal-window',
                    keyboard: false,
                    modalFade: true,
                    scope: $scope,
                    controller: function ($scope, $http,Upload,$timeout) {
                         var file=dataURItoBlob(data.myCroppedImage);
                        $scope.previewData.NEWSdate = new Date();
                       
                        $http.get('/api/getResourceData/getS3policyPlain')
                            .success(function (data) {
                                $scope.data.formDataPlain=data.data;
                            })
                            .error(function(res){
                            });
                        $scope.saveData=function(){
                                           Upload.upload({
                    url: 'http://images.myschool24x7.com/',
                    method: 'POST',
                    skipAuthorization: true,
                    data:{
                        key:new Date().toISOString()+'_'+data.newsHeading+'.png',
                        'Content-Type': file.type !== '' ? file.type : 'application/octet-stream',
                        'acl': $scope.data.formDataPlain['acl'],
                        'success_action_status':$scope.data.formDataPlain['success_action_status'],
                        'policy': $scope.data.formDataPlain['policy'],
                        'X-amz-credential':$scope.data.formDataPlain['X-amz-credential'],
                        'X-amz-algorithm':$scope.data.formDataPlain['X-amz-algorithm'],
                        'X-amz-date':$scope.data.formDataPlain['X-amz-date'],
                        'X-amz-expires': $scope.data.formDataPlain['X-amz-expires'],
                        'X-amz-signature': $scope.data.formDataPlain['X-amz-signature'],
                        file:file
                    }
                })
                    .then(function (response) {
                        var x2js = new X2JS();
                        var json = x2js.xml_str2json( response.data);
                        console.log(json)
                        $scope.previewData.myCroppedImage=null;
                        $scope.previewData.imageURL=json.PostResponse.Location;
                        $http.post('api/schoolData/news',{"data":$scope.previewData})
                        .success(function(data){
                            $state.reload();
                        })
                        .error(function(res){
                            
                        })
                        $timeout(function () {
                            file.result = response.data;
                        });
                    }, function (response) {
                        $scope.errorMsg = response.status + ': ' + response.data;
                    }, function (evt) {
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });

                        }
                        $scope.closeModal=function(){
                            modalInstance.dismiss();
                        }
                    }
                });
                }


            };
            function dataURItoBlob(dataurl) {
                var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                    bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                while(n--){
                    u8arr[n] = bstr.charCodeAt(n);
                }
                return new Blob([u8arr], {type:mime});
            }
        })

})();